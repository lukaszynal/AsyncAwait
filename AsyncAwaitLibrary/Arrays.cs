﻿namespace AsyncAwaitLibrary
{
    public class Arrays
    {
        public Task<int[]> CreateArray()
        {
            int[] array = new int[10];
            Random rnd = new Random();
            return Task.FromResult(array.Select(x => rnd.Next(-1000, 1000)).ToArray());
        }

        public Task<int[]> MultiplyArray(int[] array, int randomNumber)
        {
            ValidateInputArray(array);
            return Task.FromResult(array.Select(x => x * randomNumber).ToArray());
        }

        public Task<int[]> SortArray(int[] array)
        {
            return Task.FromResult(array.OrderBy(x => x).ToArray());
        }

        public Task<double> AverageArray(int[] array)
        {
            return Task.FromResult(array.Average());
        }

        private static void ValidateInputArray(int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new ArgumentException("Array can't be empty");
            }

            foreach (int item in array)
            {
                if (item < -1000 || item > 1000)
                {
                    throw new ArgumentException($"Number have to be in range <-1000,1000>");
                }
            }
        }
    }
}