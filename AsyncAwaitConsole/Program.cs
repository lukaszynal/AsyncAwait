﻿using AsyncAwaitLibrary;

namespace AsyncAwaitConsole
{
    public static class Program
    {
        static void Main()
        {
            _ = MainTask();
        }

        public static async Task<double> MainTask()
        {
            Arrays arr = new Arrays();

            int[] taskCreateArray = await PrintCreatedArray(arr);
            int[] taskMultiplyArray = await PrintMultipliedArray(arr, taskCreateArray);
            int[] taskSortArray = await PrintSortedArray(arr, taskMultiplyArray);
            double taskAverageArray = await PrintAverageArray(arr, taskSortArray);

            return taskAverageArray;
        }

        private static async Task<int[]> PrintCreatedArray(Arrays arr)
        {
            var result = await arr.CreateArray();
            Console.WriteLine("Created array:");
            for (int i = 0; i < result.Length; i++)
            {
                Console.WriteLine($"[{i}]: {result[i]}");
            }

            return result;
        }

        private static async Task<int[]> PrintMultipliedArray(Arrays arr, int[] array)
        {
            Random rnd = new Random();
            var randomNumber = rnd.Next(-1000, 1000);
            var result = await arr.MultiplyArray(array, randomNumber);

            Console.WriteLine($"Array multiplied by {randomNumber}");
            for (int i = 0; i < result.Length; i++)
            {
                Console.WriteLine($"[{i}]: {result[i]}");
            }

            return result;
        }

        private static async Task<int[]> PrintSortedArray(Arrays arr, int[] array)
        {
            var result = await arr.SortArray(array);
            Console.WriteLine("Sorted array:");
            for (int i = 0; i < result.Length; i++)
            {
                Console.WriteLine($"[{i}]: {result[i]}");
            }

            return result;
        }

        private static async Task<double> PrintAverageArray(Arrays arr, int[] array)
        {
            var result = await arr.AverageArray(array);
            Console.WriteLine($"Average of array: {result}:");

            return result;
        }
    }
}
